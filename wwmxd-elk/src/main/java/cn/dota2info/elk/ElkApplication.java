package cn.dota2info.elk;


import com.wwmxd.wwmxdauth.EnablewwmxdAuthClient;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableDiscoveryClient
@EnablewwmxdAuthClient
@EnableFeignClients
public class ElkApplication {
	public static void main(String[] args) {
		SpringApplication.run(ElkApplication.class, args);
	}
}
